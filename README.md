# Obsidian Everywhere

Open all Markdown files in Obsidian. This is a workaround for macOS, used because the creators of Obsidian have chosen not to enable the editing of files that are outside the program's "vaults".

## Reason
The cross-platform application [Obsidian](https://obsidian.md/) is a tool for *Personal Knowledge Management* (PKM): ordering your thoughts, gathering information, structuring varied knowledge. It uses [Markdown](https://spec.commonmark.org/current/) as the syntax for the content files. By happy accident, Obsidian (with the *Advanced Tables* plugin) turns out to be a fantastic Markdown editor, and people like me would like to use it not just for PKM but as an editor for all Markdown files.

That is [not on the development agenda](https://forum.obsidian.md/t/add-ability-to-use-obsidian-as-a-markdown-editor-on-files-outside-vault) for the application. Obsidian is meant to limit itself to files inside its "vault", a folder on the user's computer. You can have more than one vault, but no files outside them can be opened.

This script is a workaround for that: it allows users to open all Markdown files in Obsidian. ***Disclaimer***: like all workarounds, it is far from perfect and has many limitations. I hope that the ability to edit any file will eventually be added to Obsidian.

## Usage
After installation and configuration (see [below](#installation-configuration)), you can double-click any Markdown file to open it in Obsidian.

In the left sidebar, under Files, the file will show up in a folder. What you have there is actually a *link* to the real file (unless you double-clicked a file that is really inside the vault). Changes you make to that link – for example

 - moving it to a different folder in the sidebar
 - changing its name
 - deleting it

have no effect on the actual file.

Editing the *contents* in Obsidian does affect the real file.

### Details of what it does
When you double-click a Markdown file anywhere on your computer, the following happens:

1. If the file (or a link to it created earlier by 2. or 3.) is already in one of your Obsidian vaults, the file (and that vault) is opened in Obsidian.
2. Otherwise, if the file is in one of the filesystem subtrees your have *designated for special treatment*, the directory tree that leads up to the file is replicated inside the vault. For instance, if you declared `/Users/alice/Documents/from-gitlab` as a special subtree and you've double-clicked `/Users/alice/Documents/from-gitlab/obsidian-everywhere/README.md` then in the vault there will be a directory `from-gitlab` with a subdirectory `obsidian-everywhere`.

    Next, inside that directory a *symlink* will be created to the actual file. So there will be a `README.md` inside that directory, which points to the actual file that's outside the vault.

    Next, the file (and the vault) is opened. When you edit it with Obsidian, your edits are made to the actual file.
3. Otherwise, a symlink to the file will be created in the vault in a directory `Temp`, and the file (and the vault) is opened in Obsidian. If the filename conflicts with an existing link there (e.g. it is the twenty-fifth `README.md` you've opened in this way), a different filename is chosen automatically.

The way to use this is: for quick 'casual' edits to transient files, use 3; files with permanence on your system are probably in a place in your filesystem subtree that you'd like to see mirrored inside a vault so you use 2 for that subtree. An alternative usage pattern is that you let all links appear in `Temp` and then rename and move them according to some system of your own design.

If you have multiple vaults, 2. and 3. are done by default in the "current" vault, i.e. the last one you've had open. You can override this if you want the symlinks to be put in one particular vault only.

In addition to the Markdown file itself, the script also creates suitable symlinks to local files to which the file refers, such as local images included in the file or other Markdown files to which it links.

## Installation & configuration
You have to make this script into an *app* and make that the default app to open Markdown files. I would be happy to do it for you, but uploading the result might be a violation of Apple's copyright, so you'll have to go through the steps yourself:

1. Download [the script](open-in-obsidian.sh)
2. Configure it (using a text editor) – this step is optional:
    1. If files should always be opened in one particular vault (overriding it being the "current" vault), put the path of the vault in the variable `vault_where_files_must_be_opened` on line 5 of the script
    2. Put the paths of any filesystem subtrees whose structure you want replicated in the vault, in the array `subtrees_that_must_be_mirrored_in_vault` in line 6-8 (white-space-separated).

       So that part of the script might look like this:
    ```bash
        # CONFIGURATION
        vault_where_files_must_be_opened="$HOME/Obsidian vault"
        subtrees_that_must_be_mirrored_in_vault=(
	      "$HOME/Documents/from-gitlab"
	      "$HOME/Archives/Source code"
        )
    ```

    You can leave both variables empty if you prefer to.
3. Open the app *Automator* that's present on every Mac
4. Choose *New document*, then *Application*
5. With *Library* selected in the bar at the left, and *Actions* above it, find the item *Run Shell Script* in the second column and drag it into the editing area on the right
6. In the new box:
    * select `/bin/bash` from the *Shell:* dropdown at top left,
    * select `as arguments` from the *Pass input:* dropdown at top right.
7. Copy the entire contents of the script (the one you edited in step 2). Empty the box for the script in the Automator and paste the script there instead
8. Choose File... Save... and save the application with a name of your choosing (for example, *Open in Obsidian.app*). Put it in one of the *Applications* folders, for example the system folder `/Applications` or the one in your user profile `~/Applications` or some subfolder of it.
9. Find a Markdown file on your computer (the file extension is `.md`), right-click it and choose *Get Info* (or use <kbd>Cmd</kbd>+<kbd>I</kbd>)
10. In the *Open with:* dropdown of the info pane, choose *Other...* and select the app you have just made. If it's greyed out, you need to click the <kbd>Options</kbd> and select *All Applications* in the *Enable:* dropdown.
11. Back in the Info pane, below that dropdown, click on *Change All...* and confirm you want this for all `.md` files.
