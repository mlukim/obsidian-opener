#!/usr/bin/env bash
# Open a Markdown file in Obsidian

# CONFIGURATION
vault_where_files_must_be_opened=
subtrees_that_must_be_mirrored_in_vault=(

)

# Utility functions
get_linked_files() {
	# Also create symlinks to local files, like images, to which the Markdown file links
	md_dir="$(dirname "$1")"
	# Obsidian 1.0.0 doesn't display <img src="..."> but perhaps a future version will
	sed -En -e 's/.*\[[^]]*\]\(([^"]+)[^)]*\).*/\1/p' \
			-e 's/.*<img[^>]* src="([^?"]+)("|\?).*/\1/p' <"$1" |
		while IFS= read -r linktext
		do
			linked_file="$(readlink -f "$md_dir/${linktext% }")"
			# is it really a local file, and not higher up in the file tree?
			if [[ $? &&  "$linked_file" == "$md_dir"* ]]
			then
				link_dir=$2
				# create subdirs if needed
				abs_dir="$(dirname "$linked_file")"
				if [[ "$md_dir" != "$abs_dir" ]]
				then
					link_dir="$link_dir${abs_dir#$md_dir}"
					mkdir -p "$link_dir"
				fi
				linkpath="$link_dir/$(basename "$linked_file")"
				[[ ! -e "$linkpath" ]] && ln -s "$linked_file" "$linkpath"
			fi
		done
}

open_file() {
	# Thanks, https://stackoverflow.com/a/10797966/7840347
	url_encoded="$(echo -n "$1" | curl -Gso /dev/null -w %{url_effective}\
	  --data-urlencode @- "" | tr "+" " " | cut -c 3-)"
	if [[ -z $url_encoded ]]; then url_encoded="$1"; fi   # curl on macOS 13.2 has some problems
	open "obsidian://open?path=$url_encoded"
}


# Main script
IFS=$'\n' all_vaults=($(awk -F':|,|{|}|"' '{for(i=1;i<=NF;i++)if($i=="path")print$(i+3)}'\
   <"$HOME/Library/Application Support/obsidian/obsidian.json"))
default_vault="$(readlink -f "$vault_where_files_must_be_opened")" || \
default_vault="$(sed -E 's/.*"path":"([^"]+)",.*"open":true.*/\1/'\
   <"$HOME/Library/Application Support/obsidian/obsidian.json")"  # currently active vault

for file in "$@"
do
	# check for existence and readability
	if [[ (! -f "$file") || (! -r "$file") ]]
	then
		logger "OPEN-IN-OBSIDIAN warning: No readable file $file"
		continue
	fi
	abspath=$(readlink -f "$file")

	# 1. If the file is inside any vault (in place or linked), just open it
	for v in "${all_vaults[@]}"
	do
		foundpath="$(find -L "$v" -samefile "$abspath" -and ! -path "*/.trash/*")"
		if [[ $foundpath ]]
		then
			open_file "$foundpath"
			continue 2  # next input file
		fi
	done
	
	# 2. If it's in one of the folders that should be mirrored,
	#    replicate the folder's internal directory chain in the vault
	#    and put a link to the file in it; then open that
	for base in "${subtrees_that_must_be_mirrored_in_vault[@]}"
	do
		if [[ "$abspath" == "$base"* ]]
		then
			linkpath="$default_vault/$(basename "$base")${abspath#$base}"
			mkdir -p "$(dirname "$linkpath")"
			ln -s "$abspath" "$linkpath"			
			get_linked_files "$abspath" "$(dirname "$linkpath")"
			sleep 1  # delay for Obsidian to notice the new file(s)
			open_file "$linkpath"
			continue 2
		fi
	done

	# 3. In other cases, create a uniquely named symlink in the Temp folder and open it
	mkdir -p "$default_vault/Temp"
	filename="$(basename "$abspath")"
	linkpath="$default_vault/Temp/$filename"
	while [[ -e "$linkpath" ]]  # don't overwrite existing symlinks: choose a unique name
	do
		linkpath="${linkpath%.*}_$RANDOM.${linkpath##*.}"
	done
	ln -s "$abspath" "$linkpath"
	get_linked_files "$abspath" "$default_vault/Temp"
	sleep 1
	open_file "$linkpath"
done
